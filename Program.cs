﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tarea_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, cantidad_de_valores, valor, suma;
            suma = 0;
            x = 1;
            valor = 1;
            Console.Write(" ingrese la cantidad de numero que quiere sumar: ");
            cantidad_de_valores = int.Parse(Console.ReadLine());
            while (x <= cantidad_de_valores && valor > 0)
            {
                Console.WriteLine("\nIngrese un valor: ");
                valor = int.Parse(Console.ReadLine());
                suma = suma + valor;
                x++;
            }

            if (valor < 0)
            {
                Console.WriteLine("No se pueden ingresar valores negativos");
                Console.ReadKey();
            }
            else if (cantidad_de_valores < 0)
            {
                Console.WriteLine("Debe ingresar una cantidad de números que no sea negativa.");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("\nLa suma de los valores ingresados es: " + suma);
                Console.ReadKey();
            }
        }
    }

}
    
        
       


/*Crear un programa que pida números positivos al usuario, y vaya calculando
la suma de todos ellos (terminará cuando se teclea un número negativo o cero).*/


                                                                                